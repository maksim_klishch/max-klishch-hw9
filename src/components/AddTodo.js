const AddTodo = ({ onAdd }) => {

    const handleAddTodoPressEnter = (e) => {
        if(e.keyCode === 13) {
            onAdd(e.target.value)
        }
    }

    return (<>
        <input id="add-todo" onKeyDown={handleAddTodoPressEnter}/>                                                                                                      
        <label htmlFor="add-todo">{"Add New Todo"}</label>
    </>);
}

export default AddTodo;
